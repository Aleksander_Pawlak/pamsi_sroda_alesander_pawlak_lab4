#pragma once
#include <iostream>
#include <cstdlib>
#include <string>
#include "Binary_tree.h"
#include "Drzewo_ogolne.h"

using namespace std;

template<typename Typ>
void opcje_binary() {
	int wyb(0);
	bin_tr<Typ>* Tree = nullptr;
	Typ zmienn;
	do {
		cout << "Wybierz operacje:\n";
		cout << "1. Dodaj element\n";
		cout << "2. Usun element\n";
		cout << "3. Wyswietl drzewo\n";
		cout << "4. Wyswietl wysokosc drzewa\n";
		cout << "0. cofnij\n";
		cin >> wyb;
		switch (wyb) {
			case 1: {
				cout << "Podaj dodawana wartosc:\n";
				cin >> zmienn;
				bin_dodaj<Typ>(Tree, zmienn);
				break; }
			case 2: {
				cout << "Podaj wartosc ktora chcesz usunac:\n";
				cin >> zmienn;
				Tree = usun<Typ>(Tree, zmienn);
				break; }
			case 3: {
				bin_wyswietl<Typ>(Tree,0);
				break;}
			case 4: {cout << "Wysokosc drzewa wynosi: " << bin_wys<Typ>(Tree) << endl; }
			case 0: break;
			default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
	bin_kasuj<Typ>(Tree);
}


void bin_menu() {
	int wybor(0);
	do {
		cout << "Wybierz typ zmiennych drzewa:\n";
		cout << "1. int\n";
		cout << "2. float\n";
		cout << "3. string\n";
		cout << "0. cofnij\n";
		cin >> wybor;
		switch (wybor) {
			case 1: {	opcje_binary<int>();	break; }
			case 2: {opcje_binary<float>(); break; }
			case 3: {opcje_binary<string>(); break; }
			case 0: break;
			default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wybor != 0);
}

template<typename Typ>
void opcje_ogolne() {
	int wyb(0);
	Drzewo<Typ>* C = nullptr;
	Typ zmienn;
	do {
		cout << "Wybierz operacje:\n";
		cout << "1. Dodaj element\n";
		cout << "2. Usun element\n";
		cout << "3. Wyswietl drzewo\n";
		cout << "4. Wyswietl wysokosc drzewa\n";
		cout << "0. cofnij\n";
		cin >> wyb;
		switch (wyb) {
		case 1: {
			cout << "Podaj dodawana wartosc:\n";
			cin >> zmienn;
			dodaj_wybor<Typ>(C, zmienn);
			break; }
		case 2: {
			cout << "Podaj wartosc ktora chcesz usunac:\n";
			cin >> zmienn;
			C = usun<Typ>(C, zmienn);
			break; }
		case 3: {
			wyswietl_fold<Typ>(C, 0);
			//wyswietl<Typ>(C);
			break; }
		case 4: {
			int ile(0); int* wsk_ile = &ile;
			wys<Typ>(C, wsk_ile);
			cout << "Wysokosc drzewa wynosi: " << ile << endl; break; }
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wyb != 0);
	kasuj<Typ>(C);

}


void ogolne_menu() {
	int wybor(0);
	do {
		cout << "Wybierz typ zmiennych drzewa:\n";
		cout << "1. int\n";
		cout << "2. float\n";
		cout << "3. string\n";
		cout << "0. cofnij\n";
		cin >> wybor;
		switch (wybor) {
		case 1: {opcje_ogolne<int>();	break; }
		case 2: {opcje_ogolne<float>(); break; }
		case 3: {opcje_ogolne<string>(); break; }
		case 0: break;
		default: cout << "Niepoprawny wybor\n"; break;
		}
	} while (wybor != 0);
}

void menu_ost() {
	int wyb(0);
	do {
		cout << "Witam, prosze wybrac opcje:\n";
		cout << "1. Testowanie drzewa ogolnego\n";
		cout << "2. Testowanie drzewa binarnego\n";
		cout << "3. Odswiez ekran\n";
		cout << "0. Wyjscie\n";
		cin >> wyb;
		switch(wyb){
			case 1: ogolne_menu(); break;
			case 2: bin_menu(); break;
			case 3: system("cls");
			case 0: break;
			default: cout << "niepoprawna aopcja\n"; break;
		}
	} while (wyb != 0);
}