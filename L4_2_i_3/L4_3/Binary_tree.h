#pragma once

#include <iostream>
#include<cstdio>
#include  <string>
using namespace std;


template<typename Typ>
struct bin_tr {
	Typ co;
	bin_tr<Typ>* lewe;
	bin_tr<Typ>* prawe;
	bin_tr<Typ>* rodzic;
};

template<typename Typ>
bin_tr<Typ>* znajdz_min(bin_tr<Typ>* A) {
	while (A->lewe != nullptr) { A = A->lewe; }
	return A;
}

template<typename Typ>
void bin_dodaj(bin_tr<Typ> *&A, Typ we) {
	if (A == nullptr) {
		A = new bin_tr<Typ>();
		A->lewe = A->prawe = nullptr;
		A->co = we;
		A->rodzic = nullptr;
		return;
	}
	else {
		if (we > A->co) { bin_dodaj(A->prawe, we); }
		if (we <= A->co) { bin_dodaj(A->lewe, we); }
	}
}

template<typename Typ>
void bin_kasuj(bin_tr<Typ> *A) {
	if (A)
	{
		bin_kasuj(A->lewe);   // usuwamy lewe poddrzewo
		bin_kasuj(A->prawe);  // usuwamy prawe poddrzewo
		delete A;              // usuwamy sam w�ze�
	}
}

template<typename Typ>
int bin_wys(bin_tr<Typ>* A) {
	if (A == nullptr) { return 0; }
	int lewa_w = bin_wys(A->lewe);
	int prawa_w = bin_wys(A->prawe);

	return(lewa_w > prawa_w) ? lewa_w + 1 : prawa_w + 1;
}

template<typename Typ>
void bin_wyswietl2(string sp, string sn, bin_tr<Typ>* kto) {
	string s;
	string cr = "  ", cl = "  ", cp = "  ";
	cr[0] = 218; cr[1] = 196;
	cl[0] = 192; cl[1] = 196;
	cp[0] = 179;
	if (kto)
	{
		s = sp;
		if (sn == cr) s[s.length() - 2] = ' ';
		bin_wyswietl(s + cp, cr, kto->prawe);

		s = s.substr(0, sp.length() - 2);
		cout << s << sn << kto->co << endl;

		s = sp;
		if (sn == cl) s[s.length() - 2] = ' ';
		bin_wyswietl(s + cp, cl, kto->lewe);
	}
}


template<typename Typ>
void bin_wyswietl(bin_tr<Typ> *A, int gleb) {
	if (A != nullptr)
	{
		for (int i(0); i < gleb; i++)
			cout << "   ";
		cout << A->co << endl;
		/*if (A->rodzic != nullptr)
		cout << "(rodzic: " << A->rodzic->co << ")";
		cout << "  ";*/

		bin_wyswietl<Typ>(A->lewe, gleb + 1);
		bin_wyswietl<Typ>(A->prawe, gleb + 1);
	}
	//cout << endl;
}

template<typename Typ>
bin_tr<Typ>* usun(bin_tr<Typ>* A, Typ do_usun) {
	if (A == nullptr) return A;//puste
	else if (A->co > do_usun) A->lewe = usun(A->lewe, do_usun);//szuka w mniejszych wartosciach
	else if (A->co < do_usun) A->prawe = usun(A->prawe, do_usun);//szuka w wiekszych wartosciach
	else {
		if (A->lewe == nullptr && A->prawe == nullptr) {//nie ma dziecka
			/*delete A;*/A = nullptr;	return A;
		}
		else {//ma dzieci
			if(A->lewe==nullptr){//nie ma lewego dziecka
				bin_tr<Typ>* pom = A;
				A = A->prawe;
				delete pom;
			}
			else if (A->prawe == nullptr) {//nie ma prawego dziecka
				bin_tr<Typ>* pom = A;
				A = A->lewe;
				delete pom;
			}
			else {//ma dwojke dzieci
				bin_tr<Typ>* pom = znajdz_min<Typ>(A->prawe);//zamienia z prawa najmniejsza
				A->co = pom->co;
				A->prawe = usun<Typ>(A->prawe, pom->co);//usuwa powtorke w prawej
			}
		}
	}
	return A;
}
