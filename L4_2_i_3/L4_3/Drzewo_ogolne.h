#pragma once
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <string>
using namespace std;

void Flag(string co) {
	cout << "Bylem tu " << co << endl;
	system("PAUSE");
}

template<typename Typ>
struct Drzewo {
	Typ co;
	Drzewo** galaz;
	Drzewo* rodzic;
	int ill, zajete;
	Drzewo(Typ we, int ile) { co = we; galaz = new Drzewo<Typ>*[ile]; ill = ile; zajete = 0; }
};

template<typename Typ>
void usun_drzewo(Drzewo<Typ>* A) {
	for (int i(0); i < A->ill; i++) {
		delete A->galaz[i];
	}
	delete A;
}

template<typename Typ>
bool Find_wartosc(Drzewo<Typ>* A, Typ szukana) {
	if (A == nullptr)
		return false;
	if (A != nullptr) {
		if (A->co == szukana) return true;
		else if(A->zajete!=0){
			int lampka(0);
			for (int i(0); i < A->ill; i++) {
				if (Find_wartosc(A->galaz[i], szukana)) lampka = 1;
			}
			if (lampka == 1)
				return true;
			if (lampka != 1)
				return false;
		}
		else { return false; }
	}
}


template<typename Typ>
void wys(Drzewo<Typ>* A,int  *wys_wyj) {
	int que(0), pr(0), iter(0);
	if (A == nullptr) { }
	else {
		int  *i_naj = new int[A->ill];
		int *wsk_i_naj;
		*wys_wyj=*wys_wyj+1;
		for (int i(0); i < A->ill; i++) {
			wsk_i_naj = &i_naj[i];
			wys(A->galaz[i], wsk_i_naj);
					}
		for (int j(0); j < A->ill; j++) { if (i_naj[j] > pr) { pr = i_naj[j]; iter = j; } }
		wys(A->galaz[iter], wys_wyj);
	}
}

template<typename Typ>
void dodaj(Drzewo<Typ> *&A, Typ we) {

	if (A == nullptr) {
		A = (struct Drzewo<Typ>*)malloc(sizeof(struct Drzewo<Typ>));
		int il;
		cout << "ile galezi?\n"; cin >> il;
		A->co = we;
		A->ill = il;
		A->zajete = 0;
		A->rodzic = nullptr;
		A->galaz = new Drzewo<Typ>*[il];
		for (int k(0); k < il; k++) { A->galaz[k] = nullptr; }
		return;
	}
	else {
		srand(time(NULL));
		int j = rand() % (A->ill);
		if (A->galaz[j] == nullptr)
			A->zajete = A->zajete + 1;
		dodaj<Typ>(A->galaz[j], we);
		A->galaz[j]->rodzic = A;
		}
	}

template<typename Typ>
void dodaj_wybor(Drzewo<Typ> *&A, Typ we) {

	if (A == nullptr) {
		A = (struct Drzewo<Typ>*)malloc(sizeof(struct Drzewo<Typ>));
		int il;
		cout << "ile galezi?\n"; cin >> il;
		A->co = we;
		A->ill = il;
		A->zajete = 0;
		A->rodzic = nullptr;
		A->galaz = new Drzewo<Typ>*[il];
		for (int k(0); k < il; k++) { A->galaz[k] = nullptr; }
		return;
	}
	else {
		/*srand(time(NULL));
		int j = rand() % (A->ill);
		if (A->galaz[j] == nullptr)
		A->zajete = A->zajete + 1;
		dodaj<Typ>(A->galaz[j], we);
		A->galaz[j]->rodzic = A;*/
		int j, ej(0);
		cout << "Do ktorej galezi dodac?  ";
		do {
			cin >> j;
			if ((j - 1) <= A->ill && (j - 1) >= 0)
				ej = 1;
			else
				cout << "niepoprawny wybor\n";
		} while (ej != 1);
		dodaj<Typ>(A->galaz[j - 1], we);
		A->galaz[j - 1]->rodzic = A;
	}
}

template<typename Typ>
void kasuj(Drzewo<Typ> *A) {
	if (A != nullptr)
	{
		for (int i(0); i < A->ill; i++) {
			kasuj<Typ>(A->galaz[i]);
		}
		delete A;              // usuwamy sam w�ze�
	}
}

template<typename Typ>
void wyswietl(Drzewo<Typ> *A) {
	if (A != nullptr)
	{
		cout << A->co;
		if (A->rodzic != nullptr)
			cout << "(ktorego rodzic to: " << A->rodzic->co << ")";
		cout<<"  ";
		for (int i(0); i < A->ill; i++) {
			wyswietl<Typ>(A->galaz[i]);
		}
	}
	//cout << endl;
}

template<typename Typ>
void wyswietl_fold(Drzewo<Typ> *A, int gleb) {
	if (A != nullptr) {
		for (int i(0); i < gleb; i++)
			cout << "   ";
		cout << A->co << endl;
		for (int i(0); i < A->ill; i++)
			wyswietl_fold(A->galaz[i], gleb + 1);
	}
}


/*template<typename Typ>
void usun(Drzewo<Typ>* A, Typ do_usun) {
	srand(time(NULL));
	if (A == nullptr)return;//jesli A jest puste
	else if (A->co == do_usun) {//jesli A ma szukana wartosc
		Flag("ma szukana wartosc");////
		int p(0);
		for (int i(0); i < A->ill; i++) { if (A->galaz[i] == nullptr) p++; }
		Flag("sprawdzil czy jest niepuste dziecko");////
		cout << p << endl;
		if (p == A->ill) { cout << "XD\n";/*delete A;*//*kasuj<Typ>(A); return; }//A nie ma dzieci
		else {//ma dzieci
			Flag("Ma dzieci");////
			int k = rand() % (A->ill), c(0);
			while (c != 1) {
				if (A->galaz[k] != nullptr) {//szuka niepustego dziecka zeby sie z nim zamienic
					Drzewo<Typ>* pom = A->galaz[k];
					A->co = pom->co;
					cout << "ZJEM GO: " << pom->co << endl;
					usun(A->galaz[k], pom->co);
					c = 1;
				}
				else
					k = rand() % (A->ill);
			}
		}
	}
	else {// jesli A nie ma szukanej wartosci
		cout << "dalej\n";
		int k(0), lampka(0);
		while (lampka != 1 && k < A->ill) {
			if (Find_wartosc<Typ>(A->galaz[k], do_usun))
				lampka = 1;
			cout << lampka << endl;
			k++;
		}
		if (lampka == 1 && k <= A->ill)
			usun(A->galaz[k - 1], do_usun);
		else if (k == A->ill && lampka != 1)
			return;
	}
}*/
template<typename Typ>
Drzewo<Typ>* Find_Notempty(Drzewo<Typ>* A, Typ co) {//znajduje losowa niepusta galaz
	if (A == nullptr) { cout << "Galaz byla pusta\n"; return A; }
	else if(A->zajete!=0){
		int i(0), lampka(0);
		while (lampka == 0 && i < A->ill) {
			if (Find_wartosc<Typ>(A->galaz[i], co))
				lampka = 1;
			i++;
		}
		if (lampka = 1)
			return A->galaz[i - 1];
		else
			return A;
	}
	else {
		cout << "Brak pustych galezi\n";
		return A;
	}
}

template<typename Typ>
Drzewo<Typ>* usun(Drzewo<Typ>* A, Typ do_usun) {
	srand(time(NULL));
	if (A == nullptr) return A;
	else if(A->co!=do_usun){
		if (A->zajete != 0) {
			int i(0), lampka(0);
			while (lampka == 0 && i < A->ill) {
				if (Find_wartosc<Typ>(A->galaz[i], do_usun))
					lampka = 1;
				i++;
			}
			if (lampka = 1)
				A->galaz[i - 1]=usun(A->galaz[i-1],do_usun);
		}
		else return A;
	}
	else {
		if (A->zajete == 0) {
			/*delete A;*/A = nullptr; return A;
		}
		else {
			int iter(0), lampka2(0);
			while (iter < A->ill && lampka2 == 0) {
				if (A->galaz[iter] != nullptr)
					lampka2 = 1;
				iter++;
			}
			if (lampka2 == 1) {
				Drzewo<Typ>* pom = A->galaz[--iter];
				A->co = pom->co;
				A->galaz[iter] = usun(A->galaz[iter], pom->co);
			}
		}
	}
	return A;
}

