#pragma once
#include <string>

using namespace std;

template<typename Typ>
struct drzewo {
	Typ co;
	drzewo<Typ>* lewe;
	drzewo<Typ>* prawe;
	drzewo<Typ>* rodzic;
};

template<typename Typ>
void dodaj(drzewo<Typ> *&A, Typ we) {
	drzewo<Typ>* pom = A;
	if (A == nullptr) {
		A = (struct drzewo<Typ>*)malloc(sizeof(struct drzewo<Typ>));
		A->lewe = nullptr;
		A->prawe = nullptr;
		A->co = we;
		A->rodzic = pom;
		return;
	}
	else {
		if (we >= A->co) { dodaj(A->prawe, we); }
		if (we < A->co) { dodaj(A->lewe, we); }
	}
}

template<typename Typ>
void kasuj(drzewo<Typ> *A) {
	if (A)
	{
		kasuj(A->lewe);   // usuwamy lewe poddrzewo
		kasuj(A->prawe);  // usuwamy prawe poddrzewo
		delete A;              // usuwamy sam w�ze�
	}
}

template<typename Typ>
int wys(drzewo<Typ>* A) {
	if (A == nullptr) { return 0; }
	int lewa_w = wys(A->lewe);
	int prawa_w = wys(A->prawe);

	return(lewa_w > prawa_w) ? lewa_w + 1 : prawa_w + 1;
}

template<typename Typ>
void wyswietl(string sp, string sn, drzewo<Typ>* kto) {
	string s;
	string cr ="  ", cl ="  ", cp = "  ";
	cr[0] = 218; cr[1] = 196;
	cl[0] = 192; cl[1] = 196;
	cp[0] = 179;
	if (kto)
	{
		s = sp;
		if (sn == cr) s[s.length() - 2] = ' ';
		wyswietl(s + cp, cr, kto->prawe);

		s = s.substr(0, sp.length() - 2);
		cout << s << sn << kto->co << endl;

		s = sp;
		if (sn == cl) s[s.length() - 2] = ' ';
		wyswietl(s + cp, cl, kto->lewe);
	}
}



