#pragma once
#include <iostream>
#include <string>
using namespace std;

template<typename Typ, typename Typ2>
class Kolejka_prio
{
	struct Zmienn {
		Typ co;
		Typ2 klucz;
		Zmienn* nast;
		Zmienn* poprz;
		Zmienn(Typ zm, Typ2 key) { klucz = key; co = zm; nast = nullptr; }
		Zmienn() { nast = nullptr; }
	};
	Zmienn* pierwsz;
	Zmienn* ost;
	double rozm;
public:
	Kolejka_prio();
	~Kolejka_prio();
	void Dodaj(Typ we, Typ2 key);
	void wyswietl();
	void kasuj();
	long int size();
	Typ2 RemoveMin();
};


template<typename Typ, typename Typ2>
Kolejka_prio<Typ, Typ2>::Kolejka_prio()
{
	pierwsz = nullptr; ost = nullptr; rozm = 0;
}

template<typename Typ, typename Typ2>
Kolejka_prio<Typ, Typ2>::~Kolejka_prio()
{
	Zmienn* pom = new Zmienn;
	while (pierwsz != nullptr) { pom = pierwsz; pierwsz = pierwsz->nast; delete pom; }

}

template<typename Typ, typename Typ2>//wyswietla zawartosc calej kolejki
void Kolejka_prio<Typ, Typ2>::wyswietl()
{
	Zmienn* pom = pierwsz;
	if (pierwsz == nullptr) { cout << "kolejka pusta\n"; }
	else {
		while (pom != nullptr) {
			cout << "Wartosc: " << pom->co << " Klucz: " << pom->klucz << endl; pom = pom->nast;
		}
	}
	cout << "Rozm:  " << rozm << endl;
}

/*template<typename Typ, typename Typ2>//usuwa jeden element kolejki z poczatku
void Kolejka_prio<Typ, Typ2>::RemoveMin()
{
	Zmienn* pom = pierwsz;
	if (pierwsz == nullptr) {
		cout << "kolejka pusta\n";
	}
	else {
		if (pierwsz->nast == nullptr) { pierwsz = nullptr; }
		else {
			pierwsz = pierwsz->nast; pierwsz->poprz = nullptr;
		}
		rozm--;
	}
}*/

template<typename Typ, typename Typ2>//kasuje cala zawartosc kolejki
void Kolejka_prio<Typ, Typ2>::kasuj()
{
	Zmienn* pom = new Zmienn;
	while (pierwsz != nullptr) { pom = pierwsz; pierwsz = pierwsz->nast; delete pom; }
	rozm = 0;
}

template<typename Typ, typename Typ2>//zwraca rozmiar kolejki;
inline long int Kolejka_prio<Typ, Typ2>::size()
{
	return rozm;
}



template<typename Typ, typename Typ2>//dodaje element do kolejki
void Kolejka_prio<Typ, Typ2>::Dodaj(Typ we, Typ2 key)
{
	//Zmienn* pom = new Zmienn(we, key);
	if (pierwsz == nullptr) {
		pierwsz = new Zmienn(we,key);
		ost = new Zmienn(we,key);
	}
	else {
		Zmienn* tmp= pierwsz;
		Zmienn* tmp2 = tmp;
		if (pierwsz->klucz > key)
		{
			Zmienn * formerTop = pierwsz;
			pierwsz = new Zmienn(we, key);
			pierwsz->nast = formerTop;
		}
		if (pierwsz->klucz < key) {
			while (tmp && tmp->klucz < key)
			{
				tmp2 = tmp;
				tmp = tmp->nast;
			}
			tmp2->nast = new Zmienn(we, key);
			Zmienn* tmp3 = tmp2->nast;
			tmp3->nast = tmp;
		}
	}
	rozm++;
}


template<typename Typ, typename Typ2>//usuwa jeden element kolejki z poczatku
Typ2 Kolejka_prio<Typ, Typ2>::RemoveMin()
{
	Zmienn* pom = pierwsz;
	if (pierwsz == nullptr) {
		cout << "kolejka pusta\n";
	}
	else {
		if (pierwsz->nast == nullptr) { pierwsz = nullptr; }
		else {
			pierwsz = pierwsz->nast; pierwsz->poprz = nullptr;
		}
		rozm--;
		return pom->klucz;
	}
}