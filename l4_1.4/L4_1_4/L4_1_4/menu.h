#pragma once
#include <iostream>
#include <string>
#include <cstdlib>
#include "Kolejka_prio.h"
#include <ctime>
using namespace std;

template<typename Type>
Type* sortuj(Type tab[], int ile) {
	Type* tab2 = new Type[ile];
	Kolejka_prio<int, Type> Kolejk;
	for (int i(0); i < ile; i++)
		Kolejk.Dodaj(0, tab[i]);
	for (int j(0); j < ile; j++)
		tab2[j] = Kolejk.RemoveMin();
	return tab2;
}

template<typename Typ,typename Typ2>
void opcje_kolejk() {
	Typ we;
	Typ2 key;
	Kolejka_prio<Typ, Typ2> A;
	int wyb(0);
	do {
		cout << "Wybierz operacje na kolejce:\n";
		cout << "1. dodaj element\n";
		cout << "2. usun najmniejszy element\n";
		cout << "3. wyswietl zawartosc kolejki\n";
		cout << "4. Kasuj wszystkie wartosci w kolejce\n";
		cout << "5 Wyswietl ilosc elementow w kolejce\n";
		cout << "0. wstecz\n";
		cin >> wyb;
		switch (wyb) {
		case 1:cout << "Podaj wartosc elementu: "; cin >> we; cout << "\nPodaj wartosc klucza: "; cin >> key; cout << endl; A.Dodaj(we, key); break;
		case 2: void(A.RemoveMin()); break;
		case 3: A.wyswietl(); break;
		case 4: A.kasuj();
		case 5: cout << A.size() << endl;
		case 0: break;
		default: cout << "Niepoprawny wybor, sproboj jeszcze raz.\n";
		}
	} while (wyb != 0);
}


template<typename Typ>
void menu_kolejk() {
	int wyb(0);
	cout << "Wybierz typ klucza w kolejce:\n";
	cout << "1. int\n";
	cout << "2. float\n";
	cout << "3. string\n";
	cout << "0. wstecz\n";
	cin >> wyb;
		switch (wyb) {
		case 1: opcje_kolejk<Typ, int>(); break;
		case 2: opcje_kolejk<Typ, float>(); break;
		case 3: opcje_kolejk<Typ, string>(); break;
		case 0: break;
		default: cout << "Niepoprawny wybor, sproboj jeszcze raz.\n";
		}
}

template<typename Typ>
void menu_sort() {
	int wyb(0), rozm, gran;
	srand(time(NULL));
	Typ* tab;
	Typ we;
	cout << "Wybierz dzialanie\n";
	cout << "1. Sortuj tablice o okreslonym rozmiarze wypelniona losowymi zmiennymi\n";
	cout << "2. sortuj tablice z wpisanymi danymi\n";
	cout << "0. cofnij\n";
	cin >> wyb;
	switch (wyb) {
	case 1: {
		cout << "Podaj rozmiar tablicy: "; cin >> rozm; cout << "Podaj wartosc graniczna liczb: "; cin >> gran; cout << endl;
		tab = new Typ[rozm];
		for (int i(0); i < rozm; i++)
			tab[i] = rand() % gran;
		cout << "Tablica przed posortowaniem:\n";
		for (int j(0); j < rozm; j++) {
			if (j != 0 && j % 15 == 0)
				cout << endl;
			cout << tab[j] << "  ";
		}
		tab=sortuj<Typ>(tab, rozm);
		cout << "tablica posortowana:\n";
		for (int k(0); k < rozm; k++) {
			if (k != 0 && k % 15 == 0)
				cout << endl;
			cout << tab[k] << "  ";
		}
		break;
	}
	case 2: {
		cout << "Podaj rozmiar tablicy: "; cin >> rozm;
		tab = new Typ[rozm];
		cout << "wpisuj dane:\n";
		for (int i(0); i < rozm; i++) {
			cin >> we;
			cout << endl;
		}
		tab=sortuj<Typ>(tab, rozm);
		cout << "tablica posortowana:\n";
		for (int k(0); k < rozm; k++) {
			if (k != 0 && k % 15 == 0)
				cout << endl;
			cout << tab[k] << "  ";
		}
		break;
	}
	case 0: break;
	default: cout << "Niepoprawny wybor\n";
	}
}


void menu_ost() {
	int wyb(0),wyb2(0);
	do {
		cout << "Witam, prosze wybrac operacje :\n";
		cout << "1. Testowanie kolejki priorytetowej\n";
		cout << "2. Testowanie sortowania na kolejce priorytetowej\n";
		cout << "3. Odswiez ekran\n";
		cout << "0. Wyjscie\n";
		cin >> wyb;
		switch (wyb) {
		case 1: { 
			do {
				cout << "Wybierz typ zmiennych w kolejce:\n";
				cout << "1. int\n";
				cout << "2. float\n";
				cout << "3. string\n";
				cout << "0. wstecz\n";
				cin >> wyb2;
				switch (wyb2) {
				case 1: menu_kolejk<int>(); break;
				case 2: menu_kolejk<float>(); break;
				case 3: menu_kolejk<string>(); break;
				case 0: break;
				default: cout << "Niepoprawny wybor, sproboj jeszcze raz.\n";
				}
			} while (wyb2 != 0);			
			break; }
		case 2: { 
			do {
				cout << "Wybierz typ zmiennych:\n";
				cout << "1. int\n";
				cout << "2. float\n";
				cout << "0. wstecz\n";
				cin >> wyb2;
				switch (wyb2) {
				case 1: menu_sort<int>(); break;
				case 2: menu_sort<float>(); break;
				case 0: break;
				default: cout << "Niepoprawny wybor, sproboj jeszcze raz.\n";
				}
			} while (wyb2 != 0);
			break; }
		case 3: system("cls");
		case 0: break;
		default: cout << "niepoprawna aopcja\n"; break;
		}
	} while (wyb != 0);
}